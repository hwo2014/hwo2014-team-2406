package gwbot;

import gwbot.bot.GenericBot;
import gwbot.bot.impl.GoldwipfBot;
import gwbot.bot.impl.NicoBot;
import gwbot.gson.HwoJsonExclusionStrategy;
import gwbot.message.CarPositionMessage;
import gwbot.message.GameEndMessage;
import gwbot.message.GameInitMessage;
import gwbot.message.GameStartMessage;
import gwbot.message.JoinMessage;
import gwbot.message.Message;
import gwbot.message.MessageWrapper;
import gwbot.message.PingMessage;
import gwbot.message.TurboAvailableMessage;
import gwbot.message.YourCarMessage;
import gwbot.track.Piece;
import gwbot.track.Track;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.lang.reflect.Type;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import gwbot.bot.impl.BehEhBot;
import gwbot.message.AbstractJoinMessage;
import gwbot.message.CrashMessage;
import gwbot.message.DnfMessage;
import gwbot.message.FinishMessage;
import gwbot.message.JoinRaceMessage;
import gwbot.message.LapFinishedMessage;
import gwbot.message.SpawnMessage;
import gwbot.message.data.BotId;

/**
 *
 * @author Goldwipf <goldwipf@beheh.de>
 */
public final class Main {

	private static Socket socket;

	public static void main(String... args) {

		System.out.println("This is Team Goldwipf, ready for setup");

		String host = args[0];
		int port = Integer.parseInt(args[1]);
		String botName = args[2];
		String botKey = args[3];

		System.out.print("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey + "...");

		try {
			socket = new Socket(host, port);
			System.out.println(" connected");

			final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));
			final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

			System.out.print("Loading bot backend... ");

			GenericBot bot;
			String name = null;
			switch (System.getProperty("user.name")) {
				case "Nico Smeenk":
					bot = new NicoBot();
					break;
				//case "benedict":
				//	bot = new BehEhBot();
				//	break;
				default:
					bot = new GoldwipfBot();
					name = "Goldwipf";
					break;
			}
			if (name == null) {
				name = bot.getClass().getSimpleName();
			}
			System.out.println("using " + bot.getClass().getSimpleName());

			AbstractJoinMessage joinMessage = null;
			joinMessage = new JoinMessage(name, botKey);
			//joinMessage = new JoinRaceMessage(new BotId(name, botKey), 1, "imola", null);
			new Main(reader, writer, joinMessage, bot);
		}
		catch (IOException ex) {
			System.out.println("failed");
			ex.printStackTrace(System.err);
			System.exit(1);
		}
	}

	public final Gson gson;
	private final PrintWriter writer;

	public Main(final BufferedReader reader, final PrintWriter writer, final AbstractJoinMessage join, final GenericBot bot) throws IOException {

		bot.setMain(this);

		gson = new GsonBuilder().setExclusionStrategies(new HwoJsonExclusionStrategy()).create();

		this.writer = writer;
		String line = null;

		// send join message
		System.out.print("Joining race as \"" + join.getName() + "\"...");
		send(join);

		// success, if we receive answer
		while ((line = reader.readLine()) != null) {
			try {
				final MessageWrapper msgFromServer = gson.fromJson(line, MessageWrapper.class);

				if (msgFromServer.msgType.equals("join") || msgFromServer.msgType.equals("joinRace")) {
					System.out.println(" accepted");
					continue;
				}

				boolean disconnect = false;
				switch (msgFromServer.msgType) {
					case "yourCar":
						// receive details about your car
						YourCarMessage yourCarMessage = gson.fromJson(gson.toJson(msgFromServer.data), YourCarMessage.class);
						System.out.print("Receiving car data...");
						System.out.println(" name is \"" + yourCarMessage.getName() + "\", color is " + yourCarMessage.getColor());
						bot.onYourCarMessage(yourCarMessage);
						break;
					case "gameInit":
						// receive initial details about the following game
						GameInitMessage gameInitMessage = gson.fromJson(gson.toJson(msgFromServer.data), GameInitMessage.class);
						Track track = gameInitMessage.getRace().getTrack();
						List<Piece> pieces = track.getPieces();
						System.out.print("Receiving track data...");
						for (int i = 0; i < pieces.size(); i++) {
							Piece piece = pieces.get(i);
							piece.setNext(pieces.get((i + 1 + pieces.size()) % pieces.size()));
							piece.setPrevious(pieces.get((i - 1 + pieces.size()) % pieces.size()));
						}
						System.out.println(" track is \"" + track.getName() + "\" with " + track.getLanes().size() + " lanes");
						bot.onGameInitMessage(gameInitMessage);
						break;
					case "gameStart":
						// start the current game with details specified in gameInit
						GameStartMessage gameStartMessage = new GameStartMessage();
						System.out.println("Starting game!");
						bot.onGameStartMessage(gameStartMessage);
						break;
					case "carPositions":
						// receive
						Type carPositionsCollectionType = new TypeToken<ArrayList<CarPositionMessage>>() {
						}.getType();
						List<CarPositionMessage> carPositions = gson.fromJson(gson.toJson(msgFromServer.data), carPositionsCollectionType);
						currentTick = msgFromServer.gameTick;
						bot.onCarPositions(carPositions, currentTick);
						currentTick = null;
						break;
					case "turboAvailable":
						// turbo is available for a certain length
						TurboAvailableMessage turboAvailableMessage = gson.fromJson(gson.toJson(msgFromServer.data), TurboAvailableMessage.class);
						System.out.println("Turbo is available");
						bot.onTurboAvailable(turboAvailableMessage);
						break;
					case "lapFinished":
						// somebody completed a lap
						LapFinishedMessage lapFinishedMessage = gson.fromJson(gson.toJson(msgFromServer.data), LapFinishedMessage.class);
						System.out.println("\"" + lapFinishedMessage.getCar() + "\" completed a lap");
						// @todo
						break;
					case "finish":
						// somebody finished the race
						FinishMessage finishMessage = gson.fromJson(gson.toJson(msgFromServer.data), FinishMessage.class);
						System.out.println("\"" + finishMessage.getCar() + "\" finished the race");
						// @todo
						break;
					case "crash":
						// somebody crashed
						CrashMessage crashMessage = gson.fromJson(gson.toJson(msgFromServer.data), CrashMessage.class);
						System.out.println("\"" + crashMessage.getCar() + "\" crashed");
						bot.onCrash(crashMessage);
						break;
					case "spawn":
						// somebody respawned after crashing
						SpawnMessage spawnMessage = gson.fromJson(gson.toJson(msgFromServer.data), SpawnMessage.class);
						System.out.println("\"" + spawnMessage.getCar() + "\" respawned after a crash");
						bot.onSpawn(spawnMessage);
						break;
					case "dnf":
						// somebody was removed from the game
						DnfMessage dnfMessage = gson.fromJson(gson.toJson(msgFromServer.data), DnfMessage.class);
						System.out.println("\"" + dnfMessage.getCar() + "\" was disqualified");
						// ̍todo
						break;
					case "gameEnd":
						// current game has ended
						GameEndMessage gameEndMessage = gson.fromJson(gson.toJson(msgFromServer.data), GameEndMessage.class);
						System.out.println("Game has ended");
						bot.onGameEndMessage(gameEndMessage);
						break;
					case "tournamentEnd":
						disconnect = true;
						// @todo
						System.out.println("Tournament has ended");
						break;
					case "error":
						System.err.println("Error received: \"" + gson.toJson(msgFromServer.data) + "\"");
						if (lastReceived != null) {
							System.err.println("  Last received was " + gson.toJson(lastReceived));
						}
						if (lastSent != null) {
							System.err.println("  Last sent was " + lastSent.toJson(gson));
						}
						break;
					default:
						// do nothing, return ping to acknowledge
						send(new PingMessage());
						System.out.println("Unknown message received: *" + msgFromServer.msgType);
						break;
				}
				lastReceived = msgFromServer;

				if (disconnect) {
					break;
				}
			}
			catch (Exception ex) {
				System.out.println(ex.getClass().toString() + " in main loop, continuing");
				System.out.println("Stacktrace:");
				ex.printStackTrace(System.out);
			}
		}

		System.out.println("Disconnecting from server");
		socket.close();
	}

	private Message lastSent = null;
	private MessageWrapper lastReceived = null;
	private Integer currentTick = null;

	public void send(final Message message) {
		if (currentTick != null) {
			message.setGameTick(currentTick);
		}
		lastSent = message;
		writer.println(message.toJson(gson));
		writer.flush();
	}
}
