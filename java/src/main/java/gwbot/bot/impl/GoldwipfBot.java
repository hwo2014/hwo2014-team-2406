package gwbot.bot.impl;

import gwbot.Main;
import gwbot.bot.GenericBot;
import gwbot.car.Car;
import gwbot.message.CarPositionMessage;
import gwbot.message.CrashMessage;
import gwbot.message.GameEndMessage;
import gwbot.message.GameInitMessage;
import gwbot.message.GameStartMessage;
import gwbot.message.JoinMessage;
import gwbot.message.PingMessage;
import gwbot.message.SpawnMessage;
import gwbot.message.SwitchLaneMessage;
import gwbot.message.ThrottleMessage;
import gwbot.message.TurboAvailableMessage;
import gwbot.message.YourCarMessage;
import gwbot.race.Race;
import gwbot.track.Lane;
import gwbot.track.Piece;
import gwbot.track.Track;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

/**
 *
 * @author Team Goldwipf <goldwipf@beheh.de>
 */
public class GoldwipfBot extends GenericBot {

	public GoldwipfBot() {
	}

	public GoldwipfBot(Main main) {
		super(main);
	}

	private Race race;

	@Override
	public void onGameInitMessage(GameInitMessage gameInitMessage) {
		race = gameInitMessage.getRace();
		int lapCount = race.getLapCount();
		System.out.print("Final preparation... ");
		if (lapCount > 0) {
			System.out.println("will be racing for " + lapCount + " laps");
		} else {
			System.out.println("no lap count set, assuming qualifier race");
		}
		System.out.println("GoldwipfBot setup complete, ready to go");
	}

	private Car ownCar = null;

	@Override
	public void onYourCarMessage(YourCarMessage yourCarMessage) {
		ownCar = yourCarMessage.getCar();
	}

	private boolean gameRunning = false;

	@Override
	public void onGameStartMessage(GameStartMessage gameStartMessage) {
		gameRunning = true;
		// send initial throttle
		send(new ThrottleMessage(1));
	}

	@Override
	public void onGameEndMessage(GameEndMessage gameEndMessage) {
		gameRunning = false;
	}

	@Override
	public void onJoinMessage(JoinMessage joinMessage) {
	}

	private TurboAvailableMessage turboAvailableMessage = null;

	@Override
	public void onTurboAvailable(TurboAvailableMessage turboAvailableMessage) {
		this.turboAvailableMessage = turboAvailableMessage;
	}

	private boolean crashed = false;

	@Override
	public void onCrash(CrashMessage crashMessage) {
		if (crashMessage.getCar().equals(ownCar)) {
			crashed = true;
		}
	}

	@Override
	public void onSpawn(SpawnMessage spawnMessage) {
		if (spawnMessage.getCar().equals(ownCar)) {
			crashed = false;
		}
	}

	@Override
	public void onCarPositions(List<CarPositionMessage> carPositionMessages, int gameTick) {

		// don't do stuff if game is not running
		if (!gameRunning) {
			initializeSpeeds(carPositionMessages);
			send(new PingMessage());
			return;
		}

		// track speeds
		if (!trackSpeeds(carPositionMessages)) {
			System.out.println("Warning: no speed tracking at tick " + gameTick);
		}

		// don't waste time if crashed
		if (crashed) {
			send(new PingMessage());
			return;
		}

		// find our position message
		CarPositionMessage ownPositionMessage = null;
		for (CarPositionMessage carPositionMessage : carPositionMessages) {
			if (ownCar.equals(carPositionMessage.getCar())) {
				ownPositionMessage = carPositionMessage;
			}
		}
		if (ownPositionMessage == null) {
			System.out.println("Could not find own car position message at tick " + gameTick);
			send(new PingMessage());
			return;
		}

		// update current speed, friction coefficient
		// check if we should switch lanes for minimum distance
		if (checkForSwitch(race.getTrack().getPiece(ownPositionMessage.getPieceIndex()))) {
			System.out.println("Switched lane at tick " + gameTick);
			return;
		}

		// check for turbo
		// with other cars:
		// check if car is just ahead (look for next switch to overtake, or use it to break next curve)
		// check for car approaching faster from behind (brake in advance)
		// do actual speed calculation, braking in curve, ABS for drifting...
		send(new ThrottleMessage(0.4));
	}

	List<CarPositionMessage> lastPositions = new ArrayList<>();

	private void initializeSpeeds(List<CarPositionMessage> carPositionMessages) {
		lastPositions = carPositionMessages;
	}

	private boolean trackSpeeds(List<CarPositionMessage> carPositionMessages) {
		ListIterator<CarPositionMessage> iterator = carPositionMessages.listIterator();
		if (lastPositions.size() == carPositionMessages.size()) {
			Track track = race.getTrack();
			CarPositionMessage currentPositionMessage;
			CarPositionMessage lastPositionMessage;
			while (iterator.hasNext()) {
				// fetch positions
				int index = iterator.nextIndex();
				currentPositionMessage = iterator.next();
				lastPositionMessage = lastPositions.get(index);
				// do the speed tracking
				double distance = 0;
				if (lastPositionMessage.getSpeed() != null) {
					distance = lastPositionMessage.getSpeed();
				}
				Piece currentPiece = track.getPiece(currentPositionMessage.getPieceIndex());
				Piece lastPiece = track.getPiece(lastPositionMessage.getPieceIndex());
				if (currentPiece == lastPiece) {
					distance = currentPositionMessage.getInPieceDistance() - lastPositionMessage.getInPieceDistance();
				}
				currentPositionMessage.setSpeed(distance);
				// update position message with speed
				iterator.set(currentPositionMessage);
			}
			lastPositions = carPositionMessages;
			return true;
		} else {
			lastPositions = carPositionMessages;
			return false;
		}
	}

	private Piece checkedPiece = null;

	/**
	 * Check if we should send a switch message.
	 *
	 * @param currentPiece
	 * @return true, if a message was sent
	 */
	private boolean checkForSwitch(Piece currentPiece) {

		Piece nextPiece = currentPiece.next();
		if (nextPiece != checkedPiece && nextPiece.isSwitch()) {
			double angToNextSwitch = 0;
			// add all curve angles until next switch
			Piece piece = nextPiece.next();
			while (!piece.isSwitch()) {
				angToNextSwitch += piece.getAngle();
				piece = piece.next();
			}
			// if next switch is also a curve, add half of the angle
			if (nextPiece.isCurve()) {
				angToNextSwitch += (nextPiece.getAngle() / 2);
			}
			// save status
			checkedPiece = nextPiece;
			// if necessary, send the switch
			if (angToNextSwitch == 0) {
				return false;
			}
			if (angToNextSwitch > 0) {
				send(new SwitchLaneMessage(SwitchLaneMessage.Direction.RIGHT));
			} else {
				send(new SwitchLaneMessage(SwitchLaneMessage.Direction.LEFT));
			}
			return true;
		}
		return false;
	}
}
