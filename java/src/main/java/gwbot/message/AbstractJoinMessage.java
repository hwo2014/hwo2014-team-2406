package gwbot.message;

/**
 *
 * @author Benedict Etzel <developer@beheh.de>
 */
public abstract class AbstractJoinMessage extends Message {
	public abstract String getName();
}
